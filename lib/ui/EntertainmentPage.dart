import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter4/model/Response.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class EntertainmentPage extends StatefulWidget {
  @override
  _EntertainmentPageState createState() => _EntertainmentPageState();
}

class _EntertainmentPageState extends State<EntertainmentPage> {
  final String url="http://newsapi.org/v2/top-headlines?country=in&category=entertainment&apiKey=b4758b7c896b46edbf72167e67529b59";
  News news;
  bool isDataLoaded=false;


  @override
  void initState() {
    super.initState();
    getJsonData();
  }

  Future<String> getJsonData() async{
    var response=await http.get(url,headers: {"Accept": "application/json"});
    setState(() {
      var convertToJson=json.decode(response.body);
      news=News.fromJson(convertToJson);
      isDataLoaded=true;
    });

return "Success";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueGrey,
      child:isDataLoaded?ListView.builder(
        itemCount: news.articles==null?0:news.articles.length,
          itemBuilder:(BuildContext context, int index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.network('https://via.placeholder.com/500x250.png?text=${news.articles[index].title}',
              width: double.infinity,
                height: 200,
                fit: BoxFit.fill,
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Text(news.articles[index].title),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8),
                child: Text('By: ${news.articles[index].author}',
                  textAlign: TextAlign.left,
                ),
              ),
              Divider(),
            ],
          );
          })
          :Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.red,
          strokeWidth: 6,
        ),
      )
    );
  }
}
