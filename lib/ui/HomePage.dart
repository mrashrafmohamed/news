import 'package:flutter/material.dart';

import 'EntertainmentPage.dart';

class HomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}


class _MyHomePageState extends State<HomePage>  with SingleTickerProviderStateMixin{

int currentPage=0;
final pages=[
  EntertainmentPage(),
  EntertainmentPage(),
  EntertainmentPage(),
  EntertainmentPage(),
  EntertainmentPage(),

];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text("Headlines",style:TextStyle(color: Colors.white,fontSize: 18)),
      ),
      body: pages[currentPage],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.deepOrange,
        unselectedItemColor: Colors.blue,
        backgroundColor: Colors.red,
        showUnselectedLabels: true,
        currentIndex: currentPage,
        onTap: (int index){
          setState(() {
            currentPage=index;
          });
        },
        items: [
          BottomNavigationBarItem(
            backgroundColor: Colors.blueGrey,
            icon:Icon(Icons.airline_seat_recline_extra,),
            title: Text("Entertainment",)
          ),
          BottomNavigationBarItem(
              backgroundColor: Colors.blueGrey,
              icon:Icon(Icons.attach_money,),
              title: Text("Business",)
          ),
          BottomNavigationBarItem(
              backgroundColor: Colors.blueGrey,
              icon:Icon(Icons.golf_course,),
              title: Text("Sports",)
          ),
          BottomNavigationBarItem(
              backgroundColor: Colors.blueGrey,
              icon:Icon(Icons.functions,),
              title: Text("General",)
          ),
          BottomNavigationBarItem(
              backgroundColor: Colors.blueGrey,
              icon:Icon(Icons.airline_seat_recline_extra,),
              title: Text("technology",)
          ),
        ],
      ),
    );
  }
}

